import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Navigation from './components/navigation/navigation';
import Home from './components/pages/home';
import About from './components/pages/about';
import Projects from './components/pages/projects';
import Services from './components/pages/services';
import Contact from './components/pages/contact';
import { Spinner } from '@blueprintjs/core';
import {
    _AppInit,
} from './actions/app/appActions';
import './App.scss';

class App extends Component {
  componentDidMount = () => {
      const { dispatch } = this.props;
      dispatch(_AppInit());
  };

  render () {
      const { isLoading, hasErrored } = this.props;

      return (
          <Router>
              <div className="App">
                  {
                      isLoading
                          ? <Spinner /> 
                          : (
                              <div>
                                  <Navigation />
                                  <Switch>
                                      <Route exact path='/' component={Home} />
                                      <Route path='/about' component={About} />
                                      <Route path='/projects' component={Projects} />
                                      <Route path='/services' component={Services} />
                                      <Route path='/contact' component={Contact} />
                                  </Switch>
                              </div>
                          )
                  }
        
              </div>
          </Router>
      );
  }
}

App.propTypes = {
    dispatch: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    hasErrored: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
    dispatch: state.dispatch,
    isLoading: state.global.isLoading,
    hasErrored: state.global.hasErrored,
});

export default connect(mapStateToProps)(App);
