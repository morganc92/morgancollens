import { createStore, applyMiddleware } from 'redux';
import rootReducer from '../reducers/rootReducer';

import thunk from 'redux-thunk';
import logger from 'redux-logger';

// eslint-disable-next-line require-jsdoc
export default function configureStore () {
    return createStore(
        rootReducer,
        applyMiddleware(thunk, logger)
    );
}