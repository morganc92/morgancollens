import * as types from '../../actionTypes';

export const _ChangeView = () => (dispatch) => {
    dispatch({
        type: types.VIEW_CHANGE,
    });
};

export const _EnterNewView = () => (dispatch) => {
    dispatch({
        type: types.VIEW_CHANGE_SUCCESS
    });
}
