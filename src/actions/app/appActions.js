import * as types from '../actionTypes';

export const _AppInit = () => (dispatch) => {
    dispatch({
        type: types.APPLICATION_INITIALIZED,
    });
};