import { combineReducers } from 'redux';
import GlobalReducer from './app/appReducer';
import ViewReducer from './app/viewReducer';
import HomeReducer from './app/pages/home/homeReducer';

const rootReducer = combineReducers({
    global: combineReducers({
        app: GlobalReducer,
        view: ViewReducer,
        home: HomeReducer,
    })
});

export default rootReducer;
