const initialState = {
    viewEnter: false,
}

export default function ViewReducer (state = initialState, action) {
    if (typeof state === 'undefined') {
        return initialState;
    }

    switch (action.type) {
        case 'VIEW_CHANGE':
            return {
                ...state,
                viewEnter: !state.viewEnter,
            }

        default:
            return state;
    }
}