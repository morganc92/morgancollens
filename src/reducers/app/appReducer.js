
const initialState = {
    isLoading: true,
    hasErrored: false,
};

// eslint-disable-next-line require-jsdoc
export default function GlobalReducer (state = initialState, action) {
    if (typeof state === 'undefined') {
        return initialState;
    }

    switch (action.type) {
    case 'APPLICATION_INITIALIZED':
        return {
            ...state,
            isLoading: false,
        };
    default:
        return state;
    };
}