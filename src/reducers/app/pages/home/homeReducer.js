
const initialState = {
    isLoading: true,
    hasErrored: false,
}

export default function HomeReducer (state = initialState, action) {
    if (typeof state === 'undefined') {
        return initialState;
    }

    switch (action.type) {
        case 'HOME_VISITED':
            return {
                ...state,
                isLoading: false,
            }
        case 'HOME_PAGE_ERROR':
            return {
                ...state,
                hasErrored: true
            }

        default:
            return state;
    }
}