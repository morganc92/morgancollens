import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { DelayLink } from './delayLink'; 
import { Icon } from '@blueprintjs/core';
import './navigation.scss';

class Navigation extends Component {

    _navigateToNewPage = () => {

    }

    render () {
        return (
                <div className='navigation'>  
                    <div className='left'>
                        <DelayLink to='/'><span className='brand'>Morgan Collens</span></DelayLink>
                    </div>
                    <div className='right'>
                        <DelayLink to='/about' delay={500}><span className='link'><Icon className='icon' color='white' icon='person'/>Bio</span></DelayLink>
                        <DelayLink to='/projects' delay={500}><span className='link'><Icon className='icon' color='white' icon='folder-open'/>Portfolio</span></DelayLink>
                        <DelayLink to='/services' delay={500}><span className='link'><Icon className='icon' color='white' icon='wrench'/>Services</span></DelayLink>
                        <DelayLink to='/contact' delay={500}><span className='link'><Icon className='icon' color='white' icon='chat'/>Contact</span></DelayLink>
                    </div>
                </div>
        );
    }
}

export default connect()(Navigation);