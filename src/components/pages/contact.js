import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { InputGroup } from '@blueprintjs/core';
import './pages.scss';

class Contact extends Component {
    render () {
        return (
            <div className='Contact'>
                <h1 className='heading'>Contact Me</h1>

                <div className='content'>

                    <div className='details'>
                        <p>Feel free to reach out to me about whatever type of project you're looking for! If I can't help to facilitate your needs I'll always be able to help point you in the right direction or set you up with someone who can assist you!</p>
                    </div>

                    <div className='form-container'>
                
                    </div>


                </div>

                <div className='form-container'>

                </div>
                
            </div>
        );
    }
};

Contact.propTypes = {
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
    dispatch: state.dispatch,
});

export default connect(mapStateToProps)(Contact);