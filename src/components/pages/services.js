import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TechStack from './services/techStack';
import './pages.scss';

class Services extends Component {
    render () {
        return (
            <div className='Services'>
                <h1 className='heading'>Services</h1>
                
                <div className='content'>
                    <TechStack />
                
                </div>
            </div>
        );
    }
};

export default connect()(Services);