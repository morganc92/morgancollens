import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { _EnterNewView } from '../../actions/app/navigation/navActions';
import './pages.scss';

class About extends Component {

    componentDidMount = () => {
        const { dispatch } = this.props;
        dispatch(_EnterNewView());
    }

    render () {
        const { enterOrExit } = this.props;
        
        return (
            <div className={
                !enterOrExit
                    ? 'About enter'
                    : 'About exit'
            }>
                <h1 className='heading'>Who am I?</h1>

                <div className='content'>
                    <div className='split-container'>
                    
                        <div className='left'>
                            <div className='photo-container'>
                                <img className='photo' src={require('../../static/img/fb-profile.jpg')} alt='Portrait' />
                            </div>
                            <div className='details'>
                                <h1>Morgan Collens</h1>
                                <h3>Full-Stack Web and Software Engineer</h3>
                                <p>Kelowna, BC, Canada</p>
                                <div className='social'>
                                    <span className='linked-in'>
                                        <a href='https://www.linkedin.com/in/morgan-collens' alt='Linked In Profile'>
                                            <img src={require('../../static/img/linked-in-btn.png')} alt='Linked In' />
                                        </a>
                                    </span>
                                    <span className='instagram'>
                                        <a href='https://www.instagram.com/morganjcollens/' alt='Instagram Profile'>
                                            <img src={require('../../static/img/instagram-btn.png')} alt='Instagram' />
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    
                        <div className='right'>
                            <div className='bio'>
                                <div className='subtext'>
                                    <p>Originally born in Calgary Alberta and now living in the beautiful Okanagan; Morgan has trained throughout his life in a variety of both web and software development topics, in addition to maintaining an active desire to continue his progression through self study. Morgan has always had a passion for technology and creation, and while originally planned on careers in either 3D animation and audio engineer, his extreme enthusiasm for solving problems led him to programming.</p>
                                </div>

                                <h2>Hey there!</h2>
                                <p>
                                    Thanks for checking out my website! It's taken me awhile to get my own website up and running ironicly but it's finally here, and better yet, it's 100% coded by your's truly from scratch! Feel free to browse around and check out both my <span className='link'>portfolio</span> or some of the <span className='link'>services</span> I offer! I'm sure that whatever your needs are I'll have something to offer.
                                </p>
                                <p>
                                    I've been coding since my early years in middle school and I've always been eager to build my own websites and other applications. I didn't have any engineering friends or really any idea of where to start looking in that capacity, until just after high school when I met a friend at post secondary school. This friend was a key in helping me get my feet wet with some of my first courses and projects, and from there I had a running start. Since that time I've been running at full pace, learning every single thing I can about every topic I can find to help me better my knowledge in providing amazing running applications and services that are highly secure and available.
                                </p>
                                <p>
                                    There's next to no service I can't offer my clients so don't hesitate to reach out to me with any type of project or idea you have in mind and I would be happy to help you in any way I can!
                                </p>
                                <p>
                                    Thanks again for checking out my website and I will hopefully chat with you soon!
                                </p>
                                <span className='signature'>
                                   - Morgan
                                </span>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        );
    }
};

About.propTypes = {
    dispatch: PropTypes.func.isRequired,
    enterOrExit: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
    dispatch: state.dispatch,
    enterOrExit: state.global.view.enterOrExit,
});

export default connect(mapStateToProps)(About);