import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class TechTile extends Component {
    render () {
        const { icon, onClick } = this.props;
        
        return (
            <div className='tech-tile' onClick={onClick}>
                {
                    icon
                }
            </div>
        );
    }
}

TechTile.propTypes = {
    dispatch: PropTypes.func.isRequired,
    onClick: PropTypes.func.isRequired,
    icon: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
    dispatch: state.dispatch,
});

export default connect(mapStateToProps)(TechTile);