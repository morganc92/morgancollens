import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const stackValues = {
    aws: <i class="devicon-amazonwebservices-original colored"></i>,
    android: <i class="devicon-android-plain-wordmark colored"></i>,
    angular: <i class="devicon-angularjs-plain colored"></i>,
    apple: <i class="devicon-apple-original"></i>,
    babel: <i class="devicon-babel-plain colored"></i>,
    bitbucket: <i class="devicon-bitbucket-plain-wordmark colored"></i>,
    chrome: <i class="devicon-chrome-plain-wordmark"></i>,
    confluence: <i class="devicon-confluence-plain-wordmark"></i>,
    csharp: <i class="devicon-csharp-plain-wordmark"></i>,
    css3: <i class="devicon-css3-plain-wordmark colored"></i>,
    debian: <i class="devicon-debian-plain-wordmark"></i>,
    dotnet: <i class="devicon-dot-net-plain-wordmark"></i>,
    express: <i class="devicon-express-original"></i>,
    facebook: <i class="devicon-facebook-plain colored"></i>,
    firefox: <i class="devicon-firefox-plain-wordmark"></i>,
    git: <i class="devicon-git-plain"></i>,
    github: <i class="devicon-github-plain-wordmark"></i>,
    google: <i class="devicon-google-plain"></i>,
    heroku: <i class="devicon-heroku-plain-wordmark"></i>,
    html5: <i class="devicon-html5-plain-wordmark"></i>,
    javascript: <i class="devicon-javascript-plain colored"></i>,
    less: <i class="devicon-less-plain-wordmark"></i>,
    linux: <i class="devicon-linux-plain"></i>,
    mongodb: <i class="devicon-mongodb-plain-wordmark"></i>,
    mySQL: <i class="devicon-mysql-plain-wordmark"></i>,
    nodejs: <i class="devicon-nodejs-plain-wordmark"></i>,
    react: <i class="devicon-react-original-wordmark"></i>,
    sass: <i class="devicon-sass-original"></i>,
    ssh: <i class="devicon-ssh-plain-wordmark"></i>,
    ubuntu: <i class="devicon-ubuntu-plain-wordmark"></i>,
    visualstudio: <i class="devicon-visualstudio-plain"></i>,
};

class TechStack extends Component {

    render () {
        return (
            <div className='tech-stack'>

            </div>
        );
    }
}

TechStack.propTypes = {
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
    dispatch: state.dispatch,
});

export default connect(mapStateToProps)(TechStack);
