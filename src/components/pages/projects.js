import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ProjectPanel from './projects/projectPanel';
import './pages.scss';

class Projects extends Component {
    render () {
        return (
            <div className='Projects'>
                <h1 className='heading'>My Work</h1>
                <div className='content'>
                    <ProjectPanel />
                </div>
            </div>
        );
    }
};

ProjectPanel.propTypes = {
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
    dispatch: state.dispatch,
});

export default connect(mapStateToProps)(Projects);