import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Icon } from '@blueprintjs/core';
import { DelayLink } from '../navigation/delayLink';
import { _ChangeView } from '../../actions/app/navigation/navActions';
import './pages.scss';

const marquuContentValues = [
    'Come check out what I do and some of the services I can offer to suit your needs!', 'I built this website from scratch. Believe it!', 'Experienced in various web and software languages including Javascript and C#!'
];

class Home extends Component {
    state = {
        marqueeContent: null,
        extraValue: 0,
    }

    componentDidMount = () => {
        this.showExtraContent();
    };

    shouldComponentUpdate = (nextProps) => {
        const { enterOrExit } = this.props;
        if (enterOrExit !== nextProps.enterOrExit) {
            console.log('update');

            return true;
        }

        return false;
    }

    showExtraContent = () => {
        
    };

    goToNextPage = () => {
        const { dispatch } = this.props;
        dispatch(_ChangeView());
    }

    render () {
        const { enterOrExit } = this.props;
        
        return (
            <div className={
                !enterOrExit
                    ? 'page enter'
                    : 'page exit'
            }>
            
                <div className='title' onClick={this.goToNextPage}>
                    <DelayLink
                        to='/about'
                        delay={500}
                        onDelayStart={() => { this.setState({ enterOrExit: false }); }}
                    >
                        <h1>Morgan Collens</h1>
                        <h3>Full-Stack Software Engineer</h3>
                        <Icon icon='chevron-down' color='white' iconsize={40} />
                    </DelayLink>

                    <div className='marquee'>
                        <span className='text'>{}</span>
                    </div>

                    <div className='repo-link'>
                        <a href='/'>
                            <h4>Check out the code for this website on my Bitbucket!</h4>
                            <Icon className='icon' icon='chevron-right' color='white' iconSize={20} />
                        </a>
                    </div>
                </div>

                <video autoPlay muted loop className={
                    !enterOrExit
                        ? 'video video-enter' 
                        : 'video video-exit'
                }>
                    <source src={require('../../static/video/Typing_dark_03_Videvo.webm')} type="video/webm" />
                </video>

              
            </div>
        );
    }
};

Home.propTypes = {
    dispatch: PropTypes.func.isRequired,
    enterOrExit: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
    dispatch: state.dispatch,
    enterOrExit: state.global.view.enterOrExit,
});

export default connect(mapStateToProps)(Home);